# Watches a specified folder and when a file is created, it is moved to another specified folder.
# 

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import os
import json
import time

# ----- Configure below.

folder_to_track = "/home/jt/Downloads"
folder_destination = "/home/jt/Dropbox/Action Folder"

# ----- Nothing configurable beyond this comment.

class MyHandler(FileSystemEventHandler):
    i = 1

    def on_modified(self, event):
        for filename in os.listdir(folder_to_track):
            src = folder_to_track + "/" + filename
            new_destination = folder_destination + "/" + filename
            os.rename(src, new_destination)

event_handler = MyHandler()
observer = Observer()
observer.schedule(event_handler, folder_to_track, recursive=True)
observer.start()

try:
    while True:
        time.sleep(10)
except KeyboardInterrupt:
    observer.stop()
observer.join()
